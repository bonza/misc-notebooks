# About

The notebooks in this project present case studies showcasing some of the 
other packages available within my Git repository. I've attempted to keep the 
code in these notebooks to a minimum - any on-the-fly functions etc. are
contained within the `misc-notebooks/lib` directory. Below is a description of
each of the notebooks.
